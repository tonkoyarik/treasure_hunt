import os
from typing import List

import numpy

MAP_FILE_PATH = os.path.join(os.path.dirname(os.path.relpath(__file__)), "map.txt")
map_data = numpy.loadtxt(MAP_FILE_PATH, dtype=int, delimiter=" ")


class TreasureHunt:
    """Treasure Hunt class.

    Args:
        treasure_map: map object
        row: starting row
        col: starting column
    """

    def __init__(self, treasure_map: List[List[int]], row=1, col=1):
        self.treasure_map = treasure_map
        self.row = row
        self.column = col

    @property
    def _cell_value(self):
        return self.treasure_map[self.row - 1][self.column - 1]

    @property
    def _cell_coord(self):
        return self.row * 10 + self.column

    @staticmethod
    def list_to_str(data):
        return " ".join(map(str, data))

    def _get_next_row_col(self):
        return divmod(self._cell_value, 10)

    def get_treasure_path_list(self):
        """Function will return a path to treasure

        Returns:
            Treasure path (List[int])
        Raises:
            Value error if the treasure has not been found
        """

        result = list()

        for i in range(len(self.treasure_map) ** 2):
            result.append(self._cell_coord)

            if self._cell_coord == self._cell_value:
                return self.list_to_str(result)

            self.row, self.column = self._get_next_row_col()

        raise ValueError("Treasure not found")


if __name__ == "__main__":
    print(map_data)
    print(f"Treasure path: {TreasureHunt(map_data).get_treasure_path_list()}")
