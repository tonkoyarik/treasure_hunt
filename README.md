# Treasure hunt


Wrote two different implementations. The first is a functional programming approach.
The second implementation is an
object-oriented way. Functional implementation using recursion.

## Installation

1. Create a virtualenv directory `$ virtualenv venv`
2. Active virtualenv `$ source venv/bin/active`
3. Install requirements `$ pip install -r requrements.txt`

### To run functional implementation


```python functional_treasure_hunt.py```

### To run OOP implementation

```python oop_treasure_hunt.py```

### Run tests

```python -m pytest```