import os
from typing import List

import numpy

MAP_FILE_PATH = os.path.join(os.path.dirname(os.path.relpath(__file__)), "map.txt")
map_data = numpy.loadtxt(MAP_FILE_PATH, dtype=int, delimiter=" ")


def get_treasure_path_list(treasure_map: List[List[int]], n=1, row=1, col=1) -> List[int]:
    """
    Function will return a path to treasure

    Args:
        treasure_map: treasure map object
        n: number of check
        row: current row
        col: current column

    Returns:
        Treasure path (List[int])
    Raises:
        Value error if the treasure has not been found

    """
    if n > len(treasure_map) ** 2:
        raise ValueError("Treasure not found")

    cell_coord = row * 10 + col
    cell_value = treasure_map[row - 1][col - 1]

    result = [cell_coord]
    if cell_coord == cell_value:
        return result
    next_row, next_column = divmod(cell_value, 10)
    n += 1
    return result + get_treasure_path_list(
        treasure_map, n=n, row=next_row, col=next_column
    )


def get_treasure(treasure_map):
    return " ".join(map(str, get_treasure_path_list(treasure_map)))


if __name__ == "__main__":
    print(map_data)
    print(f"Treasure path: {get_treasure(map_data)}")
