import pytest

from oop_treasure_hunt import TreasureHunt


def test_find_treasure(map_with_path):
    treasure_map, expected_path = map_with_path
    assert TreasureHunt(treasure_map).get_treasure_path_list() == expected_path


def test_find_treasure_without_treasure(map_without_treasure):
    with pytest.raises(ValueError):
        TreasureHunt(map_without_treasure).get_treasure_path_list()


def test_find_treasure_infinite_loop(infinite_loop_map):
    with pytest.raises(ValueError):
        TreasureHunt(infinite_loop_map).get_treasure_path_list()
