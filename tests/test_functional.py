import pytest

from functional_treasure_hunt import get_treasure


def test_find_treasure(map_with_path):
    treasure_map, expected_path = map_with_path
    assert get_treasure(treasure_map) == expected_path


def test_find_treasure_without_treasure(map_without_treasure):
    with pytest.raises(ValueError):
        get_treasure(map_without_treasure)


def test_find_treasure_infinite_loop(infinite_loop_map):
    with pytest.raises(ValueError):
        get_treasure(infinite_loop_map)
